local socket = require "socket"
local json = require "json"

local DEBUG = false

pieces = nil
local lanes = nil
local cars = nil --all cars in this race session
local laps = nil --number of laps

local function LOGD(msg, params)
   if DEBUG then
      print(string.format(msg, params))
   end
end

local NoobBot = {}
NoobBot.__index = NoobBot

function NoobBot.create(conn, name, key)
   local bot = {}
   setmetatable(bot, NoobBot)
   bot.conn = conn
   bot.name = name
   bot.color = nil
   bot.key = key
   bot.velocity = nil
   bot.oldPieceDistance = nil
   bot.oldPieceIndex = nil
   return bot
end

function NoobBot:msg(msgtype, data)
   return json.encode.encode({msgType = msgtype, data = data})
end

function NoobBot:send(msg)
   LOGD("Sending msg: %s", msg)
   self.conn:send(msg .. "\n")
end

function NoobBot:join()
   return self:msg("join", {name = self.name, key = self.key})
end


function NoobBot:onyourcar(data)
  --{"msgType": "yourCar", "data": {
  --"name": "Schumacher",
  --"color": "red"
  --}}
  bot.color = data.color
  --print( data.color)
end

function NoobBot:ongameinit(data)
  print("ongameinit")
  pieces = data.race.track.pieces
  lanes = data.race.track.lanes
  cars = data.race.track.cars
  laps = data.race.raceSession.laps
  --maxLapTimeMs
  --quickRace (false/true)

end


function NoobBot:throttle(throttle)
   return self:msg("throttle", throttle)
end

function NoobBot:ping()
   return self:msg("ping", {})
end

function NoobBot:run()
   self:send(self:join())
   self:msgloop()
end

function NoobBot:onjoin(data)
   print("Joined")
   self:send(self:ping())
end

function NoobBot:ongamestart(data)
   print("Race started")
   self:send(self:ping())
end


function NoobBot:oncarpositions(data)
   if data then
      for index, car in ipairs(data) do
	      --print("index " .. index)
         --print("color " .. car.id.color)
	      --our car
         if car.id.name == bot.name and car.id.color == bot.color then
            local pieceIndex = car.piecePosition.pieceIndex
            if bot.oldPieceIndex ~= nil then
               if bot.oldPieceIndex == pieceIndex then
                  bot.velocity = car.piecePosition.inPieceDistance - bot.oldPieceDistance
               else
                  --numberOfPiecesDriven = pieceIndex - bot.oldPieceIndex --TODO from last to first
                  bot.velocity = car.piecePosition.inPieceDistance --distance in current piece
                  --for i=1,numberOfPiecesDriven-1
                  for i=bot.oldPieceIndex +2, pieceIndex do -- pieces in between
                     local piece = pieces[i]
                     if piece.length ~= nil then --Straight piece
                        bot.velocity = bot.velocity + piece.length --lengths of pieces in between, assumed that piece indices are in order
                     elseif piece.radius ~= nil and piece.angle ~= nil then --Bend piece
                        bot.velocity = bot.velocity + arc(piece.radius, piece.angle)
                     end
                  end
                  if bot.oldPieceIndex + 1 == nil then --from last to first piece
                     bot.oldPieceIndex = 0
                  end
                  print(bot.oldPieceIndex + 1)
                  --print("pieces length" .. #pieces)
                  local oldPiece = pieces[bot.oldPieceIndex + 1]
                  if oldPiece.length ~= nil then
                     bot.velocity = bot.velocity + oldPiece.length - bot.oldPieceDistance
                  elseif oldPiece.radius ~= nil and oldPiece.angle ~= nil then
                     bot.velocity = bot.velocity + arc(oldPiece.radius, oldPiece.angle) - bot.oldPieceDistance
                  end
               end --bot.oldPieceIndex == pieceIndex
            end --bot.oldPieceIndex ~= nil
            bot.oldPieceIndex = car.piecePosition.pieceIndex
            bot.oldPieceDistance = car.piecePosition.inPieceDistance
         end --our car
      end --for loop
   end --if data

   self:send(self:throttle(0.659))
end --function


function NoobBot:oncrash(data)
   print("Someone crashed")
   self:send(self:ping())
end

function NoobBot:ongameend(data)
   print("Race ended")
   self:send(self:ping())
end

function NoobBot:onerror(data)
   print("Error: " .. data)
   self:send(self:ping())
end

function NoobBot:msgloop()
   local line = self.conn:receive("*l")
   while line ~= nil --[[or #len > 0]] do
      LOGD("Got message: %s", line)
      local msg = json.decode.decode(line)
      if msg then
	 local msgtype = msg["msgType"]
	 local fn = NoobBot["on" .. string.lower(msgtype)]
	 if fn then
	    fn(self, msg["data"])
	 else
	    print("Got " .. msgtype)
	    self:send(self:ping())
	 end
      end
      line = self.conn:receive("*l")
   end
end

function arc( radius, angle)
	--print("arc" .. (angle/360)*2*math.pi*radius)
   return (angle/360)*2*math.pi*radius
end


if #arg == 4 then
   local host, port, name, key = unpack(arg)
   print("Connecting with parameters:")
   print(string.format("host=%s, port=%s, bot name=%s, key=%s", unpack(arg)))
   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key)
   bot:run()
elseif #arg == 0 then
   local host, port, name, key = "testserver.helloworldopen.com", 8091, "Hubaa", "Cp/1niIqCmcrrA"
   local c = assert(socket.connect(host, port))
   bot = NoobBot.create(c, name, key)
   bot:run()
else
   print("Usage: ./run host port botname botkey")
end




